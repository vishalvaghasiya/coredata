//
//  ViewController.swift
//  CoreDataExample
//
//  Created by KMSOFT on 08/03/17.
//  Copyright © 2017 CoreData. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController {

    @IBOutlet weak var myview: UIView!
    
    let managedObjectContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
    
    @IBOutlet weak var DeviceNameTextField: UITextField!
    
    @IBOutlet weak var DeviceComponyNameTextField: UITextField!
    
    @IBOutlet weak var DevicePriceTextField: UITextField!
    
    
    // Core Data Record Insert Button Action
    @IBAction func RecordSaveButton(sender: AnyObject) {
        
        if DeviceComponyNameTextField.text == "" || DeviceNameTextField == "" || DevicePriceTextField == "" {
            
            let alert = UIAlertController(title: "Error", message: "All Fields Requred", preferredStyle: .Alert)
            let action = UIAlertAction(title: "OK", style: .Default, handler: nil)
            alert.addAction(action)
            presentViewController(alert, animated: true, completion: nil)
        }else{
        
        let entityDescription = NSEntityDescription.entityForName("Entity", inManagedObjectContext: managedObjectContext)
       
        let newEntity = NSEntityDescription.insertNewObjectForEntityForName("Entity", inManagedObjectContext: managedObjectContext)
        newEntity.setValue(DeviceNameTextField.text, forKey: "devicename")
        newEntity.setValue(DeviceComponyNameTextField.text, forKey: "compony")
        newEntity.setValue(DevicePriceTextField.text, forKey: "deiveceprice")
        
        do { try managedObjectContext.save()
            
            let alert = UIAlertController(title: "Insert", message: "Success", preferredStyle: .Alert)
            let action = UIAlertAction(title: "OK", style: .Default, handler: nil)
            alert.addAction(action)
            presentViewController(alert, animated: true, completion: nil)
            
        }catch let error {
            print(" nsmanage Object Error \(error)")
        }
        }
    }

    
    // Core Data Record Display Button Action
    @IBAction func RecordDisplayButton(sender: AnyObject) {
        
         let entityDescription = NSEntityDescription.entityForName("Entity", inManagedObjectContext: managedObjectContext)
        let request: NSFetchRequest = NSFetchRequest(entityName: "Entity")
        request.entity = entityDescription
        
        let pred = NSPredicate(format: "(devicename = %@)", DeviceNameTextField.text!)
        request.predicate = pred
        
        do{
            let results = try self.managedObjectContext.executeFetchRequest(request as NSFetchRequest)
           
            if results.count > 0{
                let match = results[0] as! NSManagedObject
                
                DeviceNameTextField.text = match.valueForKey("devicename") as? String
                DeviceComponyNameTextField.text = match.valueForKey("compony") as? String
                DevicePriceTextField.text = match.valueForKey("deiveceprice") as? String
            }else{
                let alert = UIAlertController(title: "Error", message: "No Data  Found", preferredStyle: .Alert)
                let action = UIAlertAction(title: "OK", style: .Default, handler: nil)
                alert.addAction(action)
                presentViewController(alert, animated: true, completion: nil)
            }
            
        }catch let error{
            print(error)
        }
    }
    
    // # MARK- Record Delete Button Action
    @IBAction func RecordDeleteButton(sender: UIButton) {
        
        let entityDescription = NSEntityDescription.entityForName("Entity", inManagedObjectContext: managedObjectContext)
        let request: NSFetchRequest = NSFetchRequest(entityName: "Entity")
        request.entity = entityDescription
        request.returnsObjectsAsFaults  = false
        let pred = NSPredicate(format: "(devicename = %@)", DeviceNameTextField.text!)
        request.predicate = pred
        
        do{
            let results = try self.managedObjectContext.executeFetchRequest(request as NSFetchRequest)
            if results.count > 0{
                
                let match = results[0] as! NSManagedObject
                managedObjectContext.deleteObject(match)
                let alert = UIAlertController(title: "Delete", message: "Delete Successfull", preferredStyle: .Alert)
                let action = UIAlertAction(title: "OK", style: .Default, handler: nil)
                alert.addAction(action)
                presentViewController(alert, animated: true, completion: nil)
                DevicePriceTextField.text = ""
                DeviceNameTextField.text = ""
                DeviceComponyNameTextField.text = ""
                
                do {
                    try self.managedObjectContext.save()
                }catch{
                    print("Save Error")
                }

                
            }else{
                let alert = UIAlertController(title: "Error", message: "No Data Found", preferredStyle: .Alert)
                let action = UIAlertAction(title: "OK", style: .Default, handler: nil)
                alert.addAction(action)
                presentViewController(alert, animated: true, completion: nil)
                DevicePriceTextField.text = ""
                DeviceNameTextField.text = ""
                DeviceComponyNameTextField.text = ""
            }
            
        }catch let error{
            print(error)
        }
    }
    
    
    @IBAction func RecordUpdateButtonAction(sender: AnyObject) {
        let entityDescription = NSEntityDescription.entityForName("Entity", inManagedObjectContext: managedObjectContext)
        let request: NSFetchRequest = NSFetchRequest(entityName: "Entity")
        request.entity = entityDescription
        
        let pred = NSPredicate(format: "(devicename = %@)", DeviceNameTextField.text!)
        request.predicate = pred
        
        do{

             let Results = try self.managedObjectContext.executeFetchRequest(request as NSFetchRequest)
             let updateResults = Results[0] as! NSManagedObject
            
            updateResults.setValue(DeviceComponyNameTextField.text, forKey: "compony")
            updateResults.setValue(DevicePriceTextField.text, forKey: "deiveceprice")
            
           try updateResults.managedObjectContext?.save()
            
        }catch{
            
            print("Save Error!")
        }
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        myview.layer.cornerRadius = 5
        myview.clipsToBounds = true
        
    
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

